open Batteries
open Cfg

(* Analyse de vivacité *)

(* [vars_in_expr e] renvoie l'ensemble des variables qui apparaissent dans [e]. *)
let rec vars_in_expr (e: expr) = match e with
  | Ebinop (b, e1, e2) -> Set.union (vars_in_expr e1) (vars_in_expr e2)
  | Eunop (b, e) -> vars_in_expr e
  | Eint value -> Set.empty
  | Evar var_name -> Set.singleton var_name
  | Ecall (_, args) -> List.fold Set.union Set.empty (List.map vars_in_expr args)

(* [live_cfg_node node live_after] renvoie l'ensemble des variables vivantes
   avant un nœud [node], étant donné l'ensemble [live_after] des variables
   vivantes après ce nœud. *)
let live_cfg_node (node: cfg_node) (live_after: string Set.t) = match node with
  | Cassign (var_name, e, _) ->  Set.union (vars_in_expr e) (Set.remove var_name live_after)
  | Creturn e -> Set.union (vars_in_expr e) live_after
  | Ccmp (c, _, _) -> Set.union (vars_in_expr c) live_after
  | Cnop _ -> live_after
  | Ccall (_, args, _) -> List.fold Set.union live_after (List.map vars_in_expr args)

(* [live_after_node cfg n] renvoie l'ensemble des variables vivantes après le
   nœud [n] dans un CFG [cfg]. [lives] est l'état courant de l'analyse,
   c'est-à-dire une table dont les clés sont des identifiants de nœuds du CFG et
   les valeurs sont les ensembles de variables vivantes avant chaque nœud. *)
let live_after_node cfg n (lives: (int, string Set.t) Hashtbl.t) : string Set.t =
  Set.fold (fun suc acc -> Set.union (Hashtbl.find_default lives suc Set.empty) acc )  (succs cfg n) Set.empty

(* [live_cfg_nodes cfg lives] effectue une itération du calcul de point fixe.

   Cette fonction met à jour l'état de l'analyse [lives] et renvoie un booléen
   qui indique si le calcul a progressé durant cette itération (i.e. s'il existe
   au moins un nœud n pour lequel l'ensemble des variables vivantes avant ce
   nœud a changé). *)
let live_cfg_nodes cfg (lives : (int, string Set.t) Hashtbl.t) =
  Hashtbl.fold
    (fun n live acc ->
       let live_after = live_after_node cfg n lives in
       let live_updated = live_cfg_node (Hashtbl.find cfg n) live_after in
       Hashtbl.replace lives n live_updated;
       acc || not (Set.equal live live_updated)) lives false

(* [live_cfg_fun f] calcule l'ensemble des variables vivantes avant chaque nœud
   du CFG en itérant [live_cfg_nodes] jusqu'à ce qu'un point fixe soit atteint.
*)
let live_cfg_fun (f: cfg_fun) : (int, string Set.t) Hashtbl.t =
  let lives = Hashtbl.create 17 in
  Hashtbl.iter (fun n node -> Hashtbl.replace lives n Set.empty) f.cfgfunbody;
  let rec live_cfg_fun_rec cfg lives =
    if not (live_cfg_nodes cfg lives) then lives else live_cfg_fun_rec cfg lives in
  live_cfg_fun_rec f.cfgfunbody lives
