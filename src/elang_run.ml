open Elang
open Elang_gen
open Batteries
open Prog
open Utils

let binop_bool_to_int f x y = if f x y then 1 else 0

(* [eval_binop b x y] évalue l'opération binaire [b] sur les arguments [x]
   et [y]. *)
let eval_binop (b: binop) : int -> int -> int =
  match b with
  | Eadd -> fun x y -> x + y
  | Emul -> fun x y -> x * y
  | Emod -> fun x y -> x mod y
  | Ediv -> fun x y -> x / y
  | Esub -> fun x y -> x - y
  | Eclt -> fun x y -> if x < y then 1 else 0
  | Ecle -> fun x y -> if x <= y then 1 else 0
  | Ecgt -> fun x y -> if x > y then 1 else 0
  | Ecge -> fun x y -> if x >= y then 1 else 0
  | Eceq -> fun x y -> if x = y then 1 else 0
  | Ecne -> fun x y -> if x <> y then 1 else 0
  | _ -> fun x y -> 0

(* [eval_unop u x] évalue l'opération unaire [u] sur l'argument [x]. *)
let eval_unop (u: unop) : int -> int =
  match u with
  | Eneg -> fun x -> -x

(* [eval_eexpr st e] évalue l'expression [e] dans l'état [st]. Renvoie une
   erreur si besoin. *)
let rec eval_eexpr oc (ep: eprog) (st: int state) (sp: int) (typ_var: (string, typ) Hashtbl.t) (typ_fun: (string, typ list * typ) Hashtbl.t) (funvarinmem: (string, int) Hashtbl.t) (funstksz: int) (e : expr) : (int * int state) res =
  match e with
  | Ebinop (op, eexpr1, eexpr2) -> eval_eexpr oc ep st sp typ_var typ_fun funvarinmem funstksz eexpr1
    >>= fun (expr1, st) -> eval_eexpr oc ep st sp typ_var typ_fun funvarinmem funstksz eexpr2
    >>= fun (expr2, st) -> typ_expr typ_var typ_fun eexpr1
    >>= fun typ_expr1 -> typ_expr typ_var typ_fun eexpr2
    >>= fun typ_expr2 -> begin match typ_expr1 with
      | Tptr typ_ptr -> begin match typ_expr2 with
          | Tptr _ -> OK(eval_binop op expr1 expr2, st)
          | _ -> OK(eval_binop op expr1 (expr2 * (size_type typ_ptr)), st)
        end
      | _ -> begin match typ_expr2 with
          | Tptr typ_ptr -> OK(eval_binop op (expr1 * (size_type typ_ptr)) expr2, st)
          | _ -> OK(eval_binop op expr1 expr2, st)
        end
    end
  | Eunop (op, expr1) -> eval_eexpr oc ep st sp typ_var typ_fun funvarinmem funstksz expr1
    >>= fun (expr1, st) -> OK(eval_unop op expr1, st)
  | Eint n -> OK(n, st)
  | Echar c -> OK(int_of_char c, st)
  | Ecall (fname, args) ->
    let args_evaluation = List.fold (fun acc arg ->
        acc >>= fun (evaluated_args, st) ->
        eval_eexpr oc ep st sp typ_var typ_fun funvarinmem funstksz arg
        >>= fun (evaluated_arg, st) -> OK(evaluated_args@[evaluated_arg], st)
      ) (OK ([], st)) args in
    args_evaluation >>= fun (args, st) -> begin
      match find_function ep fname with
      | OK called_fun -> eval_efun oc ep st (sp+funstksz) called_fun fname typ_fun args
      | Error _ -> Builtins.do_builtin oc st.mem fname args >>= fun result -> OK (result, st)
    end >>= fun (result, st) ->
    option_to_res_bind result "Void return in expression" (fun result -> OK(result, st))
  | Evar var_name -> begin match Hashtbl.find_option st.env var_name with
      | Some value -> OK(value, st)
      | None -> begin match Hashtbl.find_option funvarinmem var_name with
          | Some addr -> typ_expr typ_var typ_fun e
            >>= fun typ_e -> Mem.read_bytes_as_int st.mem (sp+addr) (size_type typ_e)
            >>= fun value -> OK(value, st)
          | None -> Error(Format.sprintf "Unknown variable %s\n" var_name)
        end
    end
  | Eload load_expr -> eval_eexpr oc ep st sp typ_var typ_fun funvarinmem funstksz load_expr
    >>= fun (load_addr, st) -> typ_expr typ_var typ_fun load_expr
    >>= fun typ_load -> Mem.read_bytes_as_int st.mem load_addr (size_type typ_load)
    >>= fun value -> OK(value, st)
  | Eaddrof addrof_expr -> match addrof_expr with
    | Evar var_name -> begin match Hashtbl.find_option funvarinmem var_name with
        | Some addr -> OK(addr, st)
        | None -> Error(Format.sprintf "Variable %s is not on stack\n" var_name)
      end
    | Eload load_expr -> eval_eexpr oc ep st sp typ_var typ_fun funvarinmem funstksz load_expr
      >>= fun (load_addr, st) -> OK(load_addr, st)
    | _ -> Error(Format.sprintf "Unable to apply & operator to expression %s\n" (Elang_print.dump_eexpr addrof_expr))



(* [eval_einstr oc st ins] évalue l'instrution [ins] en partant de l'état [st].

   Le paramètre [oc] est un "output channel", dans lequel la fonction "print"
   écrit sa sortie, au moyen de l'instruction [Format.fprintf].

   Cette fonction renvoie [(ret, st')] :

   - [ret] est de type [int option]. [Some v] doit être renvoyé lorsqu'une
     instruction [return] est évaluée. [None] signifie qu'aucun [return] n'a eu
     lieu et que l'exécution doit continuer.

   - [st'] est l'état mis à jour. *)
and eval_einstr oc (ep: eprog) (st: int state) (sp: int) (typ_var: (string, typ) Hashtbl.t) (typ_fun: (string, typ list * typ) Hashtbl.t) (funvarinmem: (string, int) Hashtbl.t) (funstksz: int) (ins: instr):
  (int option * int state) res = match ins with
  | Iassign (var_name, value_eexpr) -> eval_eexpr oc ep st sp typ_var typ_fun funvarinmem funstksz value_eexpr
    >>= fun (value, st) -> begin match Hashtbl.find_option funvarinmem var_name with
      | Some addr -> typ_expr typ_var typ_fun value_eexpr
        >>= fun typ_e -> Mem.write_bytes st.mem (addr + sp) (split_bytes (size_type typ_e) value)
        >>= fun _ -> OK(None, st)
      | _ -> Hashtbl.replace st.env var_name value; OK(None, st)
    end
  | Iif (condition, instr1, instr2) -> eval_eexpr oc ep st sp typ_var typ_fun funvarinmem funstksz condition
    >>= fun (condition, st) -> if condition <> 0 then eval_einstr oc ep st sp typ_var typ_fun funvarinmem funstksz instr1 else eval_einstr oc ep st sp typ_var typ_fun funvarinmem funstksz instr2
  | Iwhile (condition, instr1) -> eval_eexpr oc ep st sp typ_var typ_fun funvarinmem funstksz condition
    >>= fun (condition_value, st) -> if condition_value <> 0 then eval_einstr oc ep st sp typ_var typ_fun funvarinmem funstksz instr1
      >>= fun (return_value, st) -> if Option.is_some return_value
      then OK(return_value, st)
      else eval_einstr oc ep st sp typ_var typ_fun funvarinmem funstksz (Iwhile(condition, instr1))
    else OK(None, st)
  | Iblock (block_body) -> (match block_body with
      | [] -> OK(None, st)
      | instr1::instrs -> eval_einstr oc ep st sp typ_var typ_fun funvarinmem funstksz instr1 >>= fun (return_value, st) -> if Option.is_some return_value then OK(return_value, st) else eval_einstr oc ep st sp typ_var typ_fun funvarinmem funstksz (Iblock instrs))
  | Ireturn return_expr -> eval_eexpr oc ep st sp typ_var typ_fun funvarinmem funstksz return_expr >>= fun (result, st) -> OK(Some(result), st)
  | Istore (dest_expr, store_expr) -> eval_eexpr oc ep st sp typ_var typ_fun funvarinmem funstksz dest_expr
    >>= fun (dest_ptr, st) -> typ_expr typ_var typ_fun dest_expr
    >>= fun dest_typ -> begin match dest_typ with
      | Tptr ptr_typ -> eval_eexpr oc ep st sp typ_var typ_fun funvarinmem funstksz store_expr
        >>= fun (store_value, st) -> Mem.write_bytes st.mem dest_ptr (split_bytes (size_type dest_typ) store_value)
        >>= fun _ -> OK(None, st)
      | _ -> Error (Printf.sprintf "Cannot store value in %s" (Elang_print.dump_eexpr dest_expr))
    end
  | Icall (fname, args) -> List.fold (fun acc arg ->
      acc >>= fun (evaluated_args, st) ->
      eval_eexpr oc ep st sp typ_var typ_fun funvarinmem funstksz arg >>= fun (evaluated_arg, st) ->
      OK(evaluated_args@[evaluated_arg], st)
    ) (OK ([], st)) args >>= fun (args, st) ->
    match find_function ep fname with
    | OK called_fun -> eval_efun oc ep st (sp+funstksz) called_fun fname typ_fun args
      >>= fun (_, st) -> OK(None, st)
    | Error _ -> Builtins.do_builtin oc st.mem fname args >>= fun result -> OK (None, st)


(* [eval_efun oc st f fname vargs] évalue la fonction [f] (dont le nom est
   [fname]) en partant de l'état [st], avec les arguments [vargs].

   Cette fonction renvoie un couple (ret, st') avec la même signification que
   pour [eval_einstr]. *)
and eval_efun oc (ep: eprog) (st: int state) (sp: int) ({ funargs; funbody; funvartyp; funvarinmem; funstksz}: efun)
    (fname: string) (typ_fun: (string, typ list * typ) Hashtbl.t) (vargs: int list)
  : (int option * int state) res =
  (* L'environnement d'une fonction (mapping des variables locales vers leurs
     valeurs) est local et un appel de fonction ne devrait pas modifier les
     variables de l'appelant. Donc, on sauvegarde l'environnement de l'appelant
     dans [env_save], on appelle la fonction dans un environnement propre (Avec
     seulement ses arguments), puis on restore l'environnement de l'appelant. *)
  let env_save = Hashtbl.copy st.env in
  let env = Hashtbl.create 17 in
  match List.iter2 (fun a v -> Hashtbl.replace env a v) (List.map fst funargs) vargs with
  | () ->
    eval_einstr oc ep { st with env } sp funvartyp typ_fun funvarinmem funstksz funbody >>= fun (v, st') ->
    OK (v, { st' with env = env_save })
  | exception Invalid_argument _ ->
    Error (Format.sprintf
             "E: Called function %s with %d arguments, expected %d.\n"
             fname (List.length vargs) (List.length funargs)
          )

(* [eval_eprog oc ep memsize params] évalue un programme complet [ep], avec les
   arguments [params].

   Le paramètre [memsize] donne la taille de la mémoire dont ce programme va
   disposer. Ce n'est pas utile tout de suite (nos programmes n'utilisent pas de
   mémoire), mais ça le sera lorsqu'on ajoutera de l'allocation dynamique dans
   nos programmes.

   Renvoie:

   - [OK (Some v)] lorsque l'évaluation de la fonction a lieu sans problèmes et renvoie une valeur [v].

   - [OK None] lorsque l'évaluation de la fonction termine sans renvoyer de valeur.

   - [Error msg] lorsqu'une erreur survient.
*)
let eval_eprog oc (ep: eprog) (memsize: int) (params: int list)
  : int option res =
  let st = init_state memsize in
  find_function ep "main" >>= fun f ->
  (* ne garde que le nombre nécessaire de paramètres pour la fonction "main". *)
  let n = List.length f.funargs in
  let params = take n params in
  let typ_fun = Hashtbl.create 17 in
  Hashtbl.replace typ_fun "print" ([Tint], Tvoid);
  Hashtbl.replace typ_fun "print_int" ([Tint], Tvoid);
  Hashtbl.replace typ_fun "print_char" ([Tchar], Tvoid);
  List.iter (fun (fname, f) -> match f with | Gfun f -> Hashtbl.replace typ_fun fname (List.map snd f.funargs, f.funrettyp)) ep;
  List.iter  (fun (name, value) -> Hashtbl.replace st.env name value) (List.combine (List.map fst f.funargs) params);
  eval_efun oc ep st 0 f "main" typ_fun params >>= fun (v, _) ->
  OK v
