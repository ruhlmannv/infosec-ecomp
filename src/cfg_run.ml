open Prog
open Elang
open Elang_run
open Batteries
open BatList
open Cfg
open Utils
open Builtins

let rec eval_cfgexpr oc cp st (e: expr) : (int * int state) res =
  match e with
  | Ebinop(b, e1, e2) ->
    eval_cfgexpr oc cp st e1 >>= fun (v1, st) ->
    eval_cfgexpr oc cp st e2 >>= fun (v2, st) ->
    let v = eval_binop b v1 v2 in
    OK (v, st)
  | Eunop(u, e) ->
    eval_cfgexpr oc cp st e >>= fun (v1, st) ->
    let v = (eval_unop u v1) in
    OK (v, st)
  | Eint i -> OK (i, st)
  | Evar s ->
    begin match Hashtbl.find_option st.env s with
      | Some v -> OK (v, st)
      | None -> Error (Printf.sprintf "Unknown variable %s\n" s)
    end
  | Ecall (fname, args) ->
    let args_evaluation = List.fold (fun acc arg ->
        acc >>= fun (evaluated_args, st) ->
        eval_cfgexpr oc cp st arg >>= fun (evaluated_arg, st) ->
        OK(evaluated_args@[evaluated_arg], st)
      ) (OK ([], st)) args in
    args_evaluation >>= fun (args, st) ->
    begin match find_function cp fname with
      | OK called_fun -> eval_cfgfun oc cp st fname called_fun args
      | _ -> Builtins.do_builtin oc st.mem fname args >>= fun result -> OK(result, st) end
    >>= fun (result, st) -> option_to_res_bind result "Void return in expression" (fun result -> OK(result, st))


and eval_cfginstr oc cp st ht (n: int): (int * int state) res =
  match Hashtbl.find_option ht n with
  | None -> Error (Printf.sprintf "Invalid node identifier\n")
  | Some node ->
    match node with
    | Cnop succ ->
      eval_cfginstr oc cp st ht succ
    | Cassign(v, e, succ) ->
      eval_cfgexpr oc cp st e >>= fun (i, st) ->
      Hashtbl.replace st.env v i;
      eval_cfginstr oc cp st ht succ
    | Ccmp(cond, i1, i2) ->
      eval_cfgexpr oc cp st cond >>= fun (i, st) ->
      if i = 0 then eval_cfginstr oc cp st ht i2 else eval_cfginstr oc cp st ht i1
    | Creturn(e) ->
      eval_cfgexpr oc cp st e >>= fun (e, st) ->
      OK (e, st)
    | Ccall (fname, args, succ) ->
      let args_evaluation = List.fold (fun acc arg ->
          acc >>= fun (evaluated_args, st) ->
          eval_cfgexpr oc cp st arg >>= fun (evaluated_arg, st) ->
          OK(evaluated_args@[evaluated_arg], st)
        ) (OK ([], st)) args in
      args_evaluation >>= fun (args, st) ->
      begin match find_function cp fname with
        | OK called_fun -> eval_cfgfun oc cp st fname called_fun args >>= fun (_, st) -> OK st
        | _ -> Builtins.do_builtin oc st.mem fname args >>= fun result -> OK st end
      >>= fun st -> eval_cfginstr oc cp st ht succ


and eval_cfgfun oc cp st cfgfunname { cfgfunargs;
                                      cfgfunbody;
                                      cfgentry} vargs =
  let st' = { st with env = Hashtbl.create 17 } in
  match List.iter2 (fun a v -> Hashtbl.replace st'.env a v) cfgfunargs vargs with
  | () -> eval_cfginstr oc cp st' cfgfunbody cfgentry >>= fun (v, st') ->
    OK (Some v, {st' with env = st.env})
  | exception Invalid_argument _ ->
    Error (Format.sprintf "CFG: Called function %s with %d arguments, expected %d.\n"
             cfgfunname (List.length vargs) (List.length cfgfunargs)
          )

let eval_cfgprog oc cp memsize params =
  let st = init_state memsize in
  find_function cp "main" >>= fun f ->
  let n = List.length f.cfgfunargs in
  let params = take n params in
  eval_cfgfun oc cp st "main" f params >>= fun (v, st) ->
  OK v


