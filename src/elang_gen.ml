open Ast
open Elang
open Prog
open Report
open Options
open Batteries
open Elang_print
open Utils

let tag_is_binop =
  function
    Tadd -> true
  | Tsub -> true
  | Tmul -> true
  | Tdiv -> true
  | Tmod -> true
  | Txor -> true
  | Tcle -> true
  | Tclt -> true
  | Tcge -> true
  | Tcgt -> true
  | Tceq -> true
  | Tne  -> true
  | _    -> false

let binop_of_tag =
  function
    Tadd -> Eadd
  | Tsub -> Esub
  | Tmul -> Emul
  | Tdiv -> Ediv
  | Tmod -> Emod
  | Txor -> Exor
  | Tcle -> Ecle
  | Tclt -> Eclt
  | Tcge -> Ecge
  | Tcgt -> Ecgt
  | Tceq -> Eceq
  | Tne -> Ecne
  | _ -> assert false

let typ_compatible (typ1: typ) (typ2: typ) : bool =
  match (typ1, typ2) with
  | ((Tint|Tchar), (Tint|Tchar))
  | (Tvoid, Tvoid) -> true
  | (Tptr t1, Tptr t2) -> t1 = t2
  | _ -> false

let rec typ_expr (typ_var : (string, typ) Hashtbl.t) (typ_fun : (string, typ list * typ) Hashtbl.t) (e: expr) : typ res =
  match e with
  | Ebinop (op, e1, e2) ->
    typ_expr typ_var typ_fun e1 >>= fun typ_e1 ->
    typ_expr typ_var typ_fun e2 >>= fun typ_e2 ->
    begin match typ_e1 with
      | Tptr typ_ptr -> begin match typ_e2 with
          | Tchar
          | Tint -> if op = Eadd || op = Esub then OK typ_e1 else Error (Format.sprintf "Type %s and %s are only compatible with + and -" (string_of_typ typ_e1) (string_of_typ typ_e2))
          | Tptr typ_ptr -> if (binop_is_comp op) then OK Tint else Error "Type ptr and ptr are only compatible with comparaison operations"
          | _ -> Error (Format.sprintf "Type %s and %s are not compatible with binop" (string_of_typ typ_e1) (string_of_typ typ_e2))
        end
      | Tint
      | Tchar -> begin match typ_e2 with
          | Tchar
          | Tint -> OK Tint
          | Tptr _ -> if op = Eadd then OK typ_e2 else Error (Format.sprintf "Type %s and %s are only compatible with +" (string_of_typ typ_e1) (string_of_typ typ_e2))
          | _ -> Error (Format.sprintf "Type %s and %s are not compatible with binop" (string_of_typ typ_e1) (string_of_typ typ_e2))
        end
      | _ -> Error (Format.sprintf "Type %s is not compatible with binop" (string_of_typ typ_e1))
    end
  | Eunop (_, e) -> typ_expr typ_var typ_fun e >>= fun typ_e ->
    begin match typ_e with
      | Tchar
      | Tint -> OK Tint
      | _ -> Error (Format.sprintf "Type %s is not compatible with unop" (string_of_typ typ_e))
    end
  | Evar var_name -> Hashtbl.find_option typ_var var_name |> fun var_typ ->
                     option_to_res_bind var_typ (Format.sprintf "Variable %s is not defined" var_name) (fun t -> OK(t))
  | Ecall (fname, args) -> list_map_res (fun arg -> typ_expr typ_var typ_fun arg) args >>= fun typ_args ->
    Hashtbl.find_option typ_fun fname |> fun typ_f -> begin match typ_f with
      | Some (typ_f_args, typ_ret) -> if (List.length typ_f_args) = (List.length typ_args) && List.for_all2 typ_compatible typ_f_args typ_args then OK typ_ret
        else Error (Format.sprintf "Invalid arguments type for %s" fname)
      | _ -> Error (Format.sprintf "Function %s is not defined" fname)
    end
  | Eload load_eexpr -> typ_expr typ_var typ_fun load_eexpr
    >>= fun typ_load_eexpr -> begin match typ_load_eexpr with
      | Tptr typ_ptr -> OK(typ_ptr)
      | _ -> Error "Dereferenced expression is not a pointer"
    end
  | Eaddrof addrof_eexpr -> typ_expr typ_var typ_fun addrof_eexpr
    >>= fun typ_addrof_eexpr -> OK(Tptr typ_addrof_eexpr)
  | Eint _ -> OK Tint
  | Echar _ -> OK Tchar

(* Returns a set of all variable from which the address is used in the expr e *)
let rec addr_taken_expr (e: expr) : string Set.t = match e with
  | Ebinop (_, e1, e2) -> Set.union (addr_taken_expr e1) (addr_taken_expr e2)
  | Eunop (_, e) -> addr_taken_expr e
  | Eload e -> addr_taken_expr e
  | Ecall (_, args) -> List.fold (fun acc arg -> Set.union acc (addr_taken_expr arg)) Set.empty args
  | Eaddrof (Evar var) -> Set.singleton var
  | Eaddrof e -> addr_taken_expr e
  | _ -> Set.empty

(* Returns a set of all variable from which the address is used in the instr i *)
let rec addr_taken_instr (i: instr) : string Set.t = match i with
  | Iassign (_, e) -> addr_taken_expr e
  | Iif (e, i1, i2) -> Set.union (addr_taken_expr e) (Set.union (addr_taken_instr i1) (addr_taken_instr i2))
  | Iwhile (e, i) -> Set.union (addr_taken_expr e) (addr_taken_instr i)
  | Iblock i_list -> List.fold (fun acc i -> Set.union acc (addr_taken_instr i)) Set.empty i_list
  | Ireturn e -> addr_taken_expr e
  | Icall (_, args) -> List.fold (fun acc arg -> Set.union acc (addr_taken_expr arg)) Set.empty args
  | Istore (e_dest, e_store) -> Set.union (addr_taken_expr e_dest) (addr_taken_expr e_store)

(* [make_eexpr_of_ast a] builds an expression corresponding to a tree [a]. If
   the tree is not well-formed, fails with an [Error] message. *)
let rec make_eexpr_of_ast (a: tree) : expr res =
  let res =
    match a with
    | Node(t, [expr_ast1; expr_ast2]) when tag_is_binop t -> make_eexpr_of_ast expr_ast1
      >>= fun eexpr1 -> make_eexpr_of_ast expr_ast2
      >>= fun eexpr2 -> OK(Ebinop(binop_of_tag t, eexpr1, eexpr2))
    | Node(Tneg, [expr_ast]) -> make_eexpr_of_ast expr_ast
      >>= fun eexpr -> OK(Eunop(Eneg, eexpr))
    | Node(Tint, [IntLeaf n]) -> OK(Eint n)
    | Node(Tchar, [CharLeaf c]) -> OK(Echar c)
    | Node(Tcall, [StringLeaf fname; Node(Targs, args)]) ->
      list_map_res make_eexpr_of_ast args >>= fun eexpr_args -> OK(Ecall(fname, eexpr_args))
    | StringLeaf(s) -> OK(Evar s)
    | Node(Tload, [load_expr_ast]) -> make_eexpr_of_ast load_expr_ast
      >>= fun load_eexpr -> OK(Eload(load_eexpr))
    | Node(Taddrof, [addrof_expr_ast]) -> make_eexpr_of_ast addrof_expr_ast
      >>= fun addrof_eexpr -> OK(Eaddrof(addrof_eexpr))
    | _ -> Error (Printf.sprintf "Unacceptable ast in make_eexpr_of_ast %s"
                    (string_of_ast a))
  in
  match res with
  | OK o -> res
  | Error msg -> Error (Format.sprintf "In make_eexpr_of_ast %s:\n%s"
                          (string_of_ast a) msg)

let rec make_einstr_of_ast (a: tree) (funname: string) (typ_var : (string, typ) Hashtbl.t) (typ_fun : (string, typ list * typ) Hashtbl.t ) : instr res =
  let res =
    match a with
    | Node(Tdeclarevar, [TypLeaf var_typ; StringLeaf var_name; expr_ast]) ->
      begin match Hashtbl.find_option typ_var var_name with
        | Some _ -> Error (Printf.sprintf "Redeclaration of variable %s" var_name)
        | None -> if var_typ = Tvoid
          then Error (Printf.sprintf "Variable %s declared as void" var_name)
          else begin Hashtbl.add typ_var var_name var_typ;
            if expr_ast = NullLeaf then OK(Iblock [])
            else make_einstr_of_ast expr_ast funname typ_var typ_fun end
      end
    | Node(Tassign, [Node(Tassignvar, [StringLeaf var_name;expr_ast])]) -> make_eexpr_of_ast expr_ast
      >>= fun eexpr -> typ_expr typ_var typ_fun eexpr
      >>= fun eexpr_typ -> Hashtbl.find_option typ_var var_name
                           |> fun var_typ -> option_to_res_bind var_typ (Printf.sprintf "Undeclared variable %s" var_name) (fun t -> OK(t))
                           >>= fun var_typ -> if not (typ_compatible eexpr_typ var_typ) then Error (Printf.sprintf "Expression type \"%s\" is not compatible with type \"%s\" of %s" (string_of_typ eexpr_typ) (string_of_typ var_typ) var_name)
                           else OK(Iassign(var_name, eexpr))
    | Node(Tif, [expr_ast; instr_ast1; instr_ast2]) -> make_eexpr_of_ast expr_ast
      >>= fun eexpr -> typ_expr typ_var typ_fun eexpr
      >>= fun eexpr_typ -> make_einstr_of_ast instr_ast1 funname typ_var typ_fun
      >>= fun einstr1 -> make_einstr_of_ast instr_ast2 funname typ_var typ_fun
      >>= fun einstr2 -> OK(Iif(eexpr, einstr1, einstr2))
    | Node(Twhile, [expr_ast; instr_ast]) -> make_eexpr_of_ast expr_ast
      >>= fun eexpr -> typ_expr typ_var typ_fun eexpr
      >>= fun eexpr_typ -> make_einstr_of_ast instr_ast funname typ_var typ_fun
      >>= fun einstr -> OK(Iwhile(eexpr, einstr))
    | Node(Tblock, list_expr_ast) ->
      (match list_map_res (fun a -> make_einstr_of_ast a funname typ_var typ_fun) list_expr_ast with
       |OK einstr_list -> OK(Iblock einstr_list)
       |Error s -> Error s)
    | Node(Tcall, [StringLeaf fname; Node(Targs, args)]) ->
      list_map_res make_eexpr_of_ast args
      >>= fun eexpr_args -> list_map_res (fun arg -> typ_expr typ_var typ_fun arg) eexpr_args
      >>= fun args_typ -> Hashtbl.find_option typ_fun fname
                          |> fun f_typ -> option_to_res_bind f_typ (Format.sprintf "Function %s is not defined" fname)
                            (fun (f_args_typ, ret_typ) ->
                               if List.for_all2 typ_compatible f_args_typ args_typ then OK(Icall(fname, eexpr_args))
                               else Error (Format.sprintf "Invalid arguments type for %s" fname)
                            )
    | Node(Treturn, [expr_ast]) -> make_eexpr_of_ast expr_ast
      >>= fun eexpr -> typ_expr typ_var typ_fun eexpr
      >>= fun eexpr_typ -> let ret_typ = (match (Hashtbl.find_option typ_fun funname) with None -> Tint | Some (_, ret_typ) -> ret_typ) in
      if typ_compatible ret_typ eexpr_typ
      then OK(Ireturn eexpr)
      else Error (Printf.sprintf "Invalid return type in %s got %s expected %s" funname (string_of_typ eexpr_typ) (string_of_typ ret_typ))
    | Node(Tstore, [dest_expr_ast; store_expr_ast]) -> make_eexpr_of_ast dest_expr_ast
      >>= fun dest_eexpr -> make_eexpr_of_ast store_expr_ast
      >>= fun store_eexpr -> typ_expr typ_var typ_fun dest_eexpr
      >>= fun typ_dest_eexpr -> typ_expr typ_var typ_fun store_eexpr
      >>= fun typ_store_eexpr -> begin match typ_dest_eexpr with
        | Tptr typ_ptr -> if typ_compatible typ_ptr typ_store_eexpr
          then OK(Istore(dest_eexpr, store_eexpr))
          else Error (Printf.sprintf "Invalid type in store: %s and %s are not compatible"
                        (string_of_typ typ_dest_eexpr) (string_of_typ typ_store_eexpr))
        | _ -> Error "Cannot store in non pointer expression"
      end
    | _ -> Error (Printf.sprintf "Unacceptable ast in make_einstr_of_ast %s"
                    (string_of_ast a))
  in
  match res with
    OK o -> res
  | Error msg -> Error (Format.sprintf "In make_einstr_of_ast %s:\n%s"
                          (string_of_ast a) msg)

let make_ident (a: tree) : (string * typ) res =
  match a with
  | Node (Targ, [TypLeaf arg_typ; s]) ->
    OK (string_of_stringleaf s, arg_typ)
  | a -> Error (Printf.sprintf "make_ident: unexpected AST: %s"
                  (string_of_ast a))

let make_fundef_of_ast (a: tree) (typ_fun: (string, typ list * typ) Hashtbl.t ) : (string * efun) option res =
  match a with
  | Node (Tfundef, [TypLeaf ret_typ; Node(Tfunname,[StringLeaf fname]); Node (Tfunargs, fargs); body_node ]) -> list_map_res make_ident fargs
    >>= fun fargs -> begin match Hashtbl.find_option typ_fun fname with (*Check type if the function as already been declared*)
      | Some (declared_args_typ, declared_ret_typ) ->
        if declared_args_typ = (List.map snd fargs) && declared_ret_typ = ret_typ then OK ()
        else Error (Printf.sprintf "Function %s has been declared with a different signature" fname )
      | None -> OK () end
    >>= fun _ -> Hashtbl.replace typ_fun fname ((List.map snd fargs), ret_typ);
    begin match body_node with
      | Node(Tfunbody, [fbody]) ->
        let typ_var = Hashtbl.create 17 in
        List.iter (fun (arg_name, arg_typ) -> Hashtbl.replace typ_var arg_name arg_typ) fargs;
        make_einstr_of_ast fbody fname typ_var typ_fun
        >>= fun fbody -> let addr_taken = addr_taken_instr fbody in
        let fvarinmem = Hashtbl.create 17 in
        (* Populate fvarinmem with all variables on the stack and store stack size *)
        let fstksz = Set.fold
            (fun var acc -> let var_size = size_type (Hashtbl.find_default typ_var var Tvoid) in Hashtbl.replace fvarinmem var acc; acc+var_size)
            addr_taken 0 in
        OK(Some (fname, {funargs=fargs; funbody = fbody; funvartyp = typ_var; funrettyp = ret_typ; funvarinmem = fvarinmem; funstksz = fstksz}))
      | _ -> OK None
    end
  | _ ->
    Error (Printf.sprintf "make_fundef_of_ast: Expected a Tfundef, got %s."
             (string_of_ast a))

let make_eprog_of_ast (a: tree) : eprog res =
  let typ_fun = Hashtbl.create 17 in
  Hashtbl.replace typ_fun "print" ([Tint], Tvoid);
  Hashtbl.replace typ_fun "print_int" ([Tint], Tvoid);
  Hashtbl.replace typ_fun "print_char" ([Tchar], Tvoid);
  match a with
  | Node (Tlistglobdef, l) ->
    List.fold (fun acc a -> acc >>= fun acc -> make_fundef_of_ast a typ_fun >>= fun fundef -> match fundef with
      | Some (fname, efun) -> OK (acc@[(fname, Gfun efun)])
      | None -> OK acc
      )
      (OK []) l
  | _ ->
    Error (Printf.sprintf "make_fundef_of_ast: Expected a Tlistglobdef, got %s."
             (string_of_ast a))

let pass_elang ast =
  match make_eprog_of_ast ast with
  | Error msg ->
    record_compile_result ~error:(Some msg) "Elang";
    Error msg
  | OK  ep ->
    dump !e_dump dump_e ep (fun file () ->
        add_to_report "e" "E" (Code (file_contents file))); OK ep
