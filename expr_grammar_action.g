tokens SYM_EOF SYM_IDENTIFIER<string> SYM_INTEGER<int> SYM_CHARACTER<char> SYM_PLUS SYM_MINUS SYM_ASTERISK SYM_DIV SYM_MOD
tokens SYM_LPARENTHESIS SYM_RPARENTHESIS SYM_LBRACE SYM_RBRACE
tokens SYM_ASSIGN SYM_SEMICOLON SYM_RETURN SYM_IF SYM_WHILE SYM_ELSE SYM_COMMA
tokens SYM_EQUALITY SYM_NOTEQ SYM_LT SYM_LEQ SYM_GT SYM_GEQ
tokens SYM_VOID SYM_CHAR SYM_INT SYM_AMPERSAND
non-terminals S INSTR INSTRS BLOCK ELSE EXPR FACTOR
non-terminals LPARAMS REST_PARAMS
non-terminals IDENTIFIER
non-terminals FUNDEF FUNDEFS
non-terminals FUNCALL L_FUNARGS REST_FUNARGS
non-terminals FUNCALL_OR_ASSIGN FUNCALL_OR_VAR
non-terminals ADD_EXPRS ADD_EXPR
non-terminals MUL_EXPRS MUL_EXPR
non-terminals CMP_EXPRS CMP_EXPR
non-terminals EQ_EXPRS EQ_EXPR
non-terminals DECLARE_VAR DECLARE_FUN
non-terminals TYPE TYPE_PTR LOAD_FACTOR

axiom S
{

  open Symbols
  open Ast
  open BatPrintf
  open BatBuffer
  open Batteries
  open Utils

  let resolve_associativity (term:tree) (other:tree list) = List.fold_left
      (fun acc next_term -> match next_term with
       | Node(tag, tree) -> Node(tag, acc::tree)
       | _ -> acc)
      term other


}

rules
S -> FUNDEFS SYM_EOF {  Node (Tlistglobdef, $1) }
FUNDEFS -> {[]}
FUNDEFS -> FUNDEF FUNDEFS { $1::$2 }
FUNDEF -> TYPE IDENTIFIER SYM_LPARENTHESIS LPARAMS SYM_RPARENTHESIS DECLARE_FUN {Node(Tfundef, [$1;Node(Tfunname,[$2]);Node(Tfunargs,$4)]@$6)}
LPARAMS -> {[]}
LPARAMS -> TYPE IDENTIFIER REST_PARAMS {Node(Targ,[$1;$2])::$3}
REST_PARAMS -> {[]}
REST_PARAMS -> SYM_COMMA LPARAMS {$2}
DECLARE_FUN -> SYM_SEMICOLON {[NullLeaf]}
DECLARE_FUN -> INSTR {[Node(Tfunbody,[$1])]}
INSTRS -> {[]}
INSTRS -> INSTR INSTRS {$1::$2}
INSTR -> SYM_IF SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS BLOCK ELSE {Node(Tif,[$3;$5;$6])}
INSTR -> SYM_WHILE SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS INSTR {Node(Twhile, [$3;$5])}
INSTR -> SYM_RETURN EXPR SYM_SEMICOLON {Node(Treturn, [$2])}
INSTR -> IDENTIFIER FUNCALL_OR_ASSIGN {$2 $1}
INSTR -> TYPE IDENTIFIER DECLARE_VAR {$3 $1 $2}
INSTR -> BLOCK {$1}
INSTR -> SYM_ASTERISK LOAD_FACTOR SYM_ASSIGN EXPR SYM_SEMICOLON {Node(Tstore, [$2; $4])}
DECLARE_VAR -> SYM_SEMICOLON {fun typ identifier -> Node(Tdeclarevar, [typ; identifier; NullLeaf])}
DECLARE_VAR -> SYM_ASSIGN EXPR SYM_SEMICOLON {fun typ identifier -> Node(Tdeclarevar, [typ; identifier; Node(Tassign,[Node(Tassignvar, [identifier;$2])])])}
FUNCALL_OR_ASSIGN -> SYM_ASSIGN EXPR SYM_SEMICOLON {fun identifier -> Node(Tassign,[Node(Tassignvar, [identifier;$2])])}
FUNCALL_OR_ASSIGN -> FUNCALL SYM_SEMICOLON {$1}
BLOCK -> SYM_LBRACE INSTRS SYM_RBRACE {Node(Tblock, $2)}
ELSE -> SYM_ELSE BLOCK {$2}
ELSE -> {Node(Tblock, [])}
EXPR -> EQ_EXPR EQ_EXPRS {resolve_associativity $1 $2}
EQ_EXPR -> CMP_EXPR CMP_EXPRS {resolve_associativity $1 $2}
CMP_EXPR -> ADD_EXPR ADD_EXPRS {resolve_associativity $1 $2}
ADD_EXPR -> MUL_EXPR MUL_EXPRS {resolve_associativity $1 $2}
MUL_EXPR -> FACTOR {$1}
IDENTIFIER -> SYM_IDENTIFIER {StringLeaf($1)}
FACTOR -> SYM_INTEGER {Node(Tint,[IntLeaf($1)])}
FACTOR -> SYM_CHARACTER {Node(Tchar,[CharLeaf($1)])}
FACTOR -> IDENTIFIER FUNCALL_OR_VAR {$2 $1}
FACTOR -> SYM_MINUS FACTOR {Node(Tneg, [$2])}
FACTOR -> SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS {$2}
FACTOR -> SYM_AMPERSAND IDENTIFIER {Node(Taddrof, [$2])}
FACTOR -> SYM_AMPERSAND SYM_ASTERISK LOAD_FACTOR {Node(Taddrof, [Node(Tload, [$2])])}
FACTOR -> SYM_ASTERISK LOAD_FACTOR {Node(Tload, [$2])}
LOAD_FACTOR -> IDENTIFIER {$1}
LOAD_FACTOR -> SYM_ASTERISK LOAD_FACTOR {Node(Tload, [$2])}
LOAD_FACTOR -> SYM_LPARENTHESIS EXPR SYM_RPARENTHESIS {$2}
ADD_EXPRS -> {[]}
ADD_EXPRS -> SYM_PLUS ADD_EXPR ADD_EXPRS {Node(Tadd, [$2]) :: $3}
ADD_EXPRS -> SYM_MINUS ADD_EXPR ADD_EXPRS {Node(Tsub, [$2]) :: $3}
MUL_EXPRS -> {[]}
MUL_EXPRS -> SYM_ASTERISK MUL_EXPR MUL_EXPRS {Node(Tmul, [$2]) :: $3}
MUL_EXPRS -> SYM_DIV MUL_EXPR MUL_EXPRS {Node(Tdiv, [$2]) :: $3}
MUL_EXPRS -> SYM_MOD MUL_EXPR MUL_EXPRS {Node(Tmod, [$2]) :: $3}
CMP_EXPRS -> {[]}
CMP_EXPRS -> SYM_LT CMP_EXPR CMP_EXPRS {Node(Tclt, [$2]) :: $3}
CMP_EXPRS -> SYM_LEQ CMP_EXPR CMP_EXPRS {Node(Tcle, [$2]) :: $3}
CMP_EXPRS -> SYM_GT CMP_EXPR CMP_EXPRS {Node(Tcgt, [$2]) :: $3}
CMP_EXPRS -> SYM_GEQ CMP_EXPR CMP_EXPRS {Node(Tcge, [$2]) :: $3}
EQ_EXPRS -> {[]}
EQ_EXPRS -> SYM_EQUALITY EQ_EXPR EQ_EXPRS {Node(Tceq, [$2]) :: $3}
EQ_EXPRS -> SYM_NOTEQ EQ_EXPR EQ_EXPRS {Node(Tne, [$2]) :: $3}
FUNCALL_OR_VAR -> {fun identifier -> identifier}
FUNCALL_OR_VAR -> FUNCALL {$1}
FUNCALL -> SYM_LPARENTHESIS L_FUNARGS SYM_RPARENTHESIS {fun identifier -> Node(Tcall, identifier::[Node(Targs, $2)])}
L_FUNARGS -> {[]}
L_FUNARGS -> EXPR REST_FUNARGS {$1::$2}
REST_FUNARGS -> {[]}
REST_FUNARGS -> SYM_COMMA L_FUNARGS {$2}
TYPE -> SYM_CHAR TYPE_PTR {TypLeaf($2 Prog.Tchar)}
TYPE -> SYM_INT TYPE_PTR {TypLeaf($2 Prog.Tint)}
TYPE -> SYM_VOID TYPE_PTR {TypLeaf($2 Prog.Tvoid)}
TYPE_PTR -> {fun typ -> typ}
TYPE_PTR -> SYM_ASTERISK TYPE_PTR {fun typ -> $2 (Prog.Tptr typ)}
